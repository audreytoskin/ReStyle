"use strict";

// store contains persisted state.
var store = chrome.storage.sync;

// styles contains style objects.
var styles = [];

var onUpdated = chrome.tabs.onUpdated;
if (onUpdated.hasListener(reStyle) !== true) {
    onUpdated.addListener(reStyle);
}

var onChanged = chrome.storage.onChanged;
if (onChanged.hasListener(storeChanged) !== true) {
    onChanged.addListener(storeChanged);
}

store.get(null, initStorage);

// initStorage checks is sync storage is available, and falls back to local storage if not.
function initStorage(ok) {
    if (!ok) {
        store = chrome.storage.local;
    }
    storeChanged();
}

// storeChanged handles configuration changes in the store.
function storeChanged() {
    store.get(null, update);
}

// update replaces the style descriptors.
function update(conf) {
    if (conf.stylesheet) {
        var sections = conf.stylesheet.split(/(?:^|\n)\/\*\*\*(?:$|\n)/);
        styles = parseSections(sections);
    }
}

// parseStylesheet returns an section descriptions parsed from stylesheet.
function parseSections(sections) {
    var i, l, styles = [];
    for (i = 1, l = sections.length; i < l; i += 1) {
        var parts = sections[i].split(/(?:^|\n)\*\*\*\/(?:$|\n)/);
        if (parts.length !== 2) {
            console.warn("ReStyle: ignoring section #" + i + ": must contain '***/' exactly once.");
            continue;
        }
        styles.push({
            "rule": getParts(parts[0].split(/\n/)),
            "code": parts[1]
        });
    }
    return styles;
}

// getParts transforms an array of lines into an array of parts by splitting each line on /  +/ (at least two spaces).
function getParts(lines) {
    var i, l, parts = [];
    for (i = 0, l = lines.length; i < l; i += 1) {
        if (isEmpty(lines[i])) continue;
        parts.push(lines[i].split(/  +/));
    }
    return parts;
}

// isEmpty returns true if a string only contains whitespaces.
function isEmpty(s) {
    return /^\s*$/.test(s);
}

// reStyle injects css code of matching style sections.
function reStyle(id, changed, tab) {
    if (!changed || !changed.url) return;

    var i, l, style, sections = [];
    for (i = 0, l = styles.length; i < l; i += 1) {
        style = styles[i];
        if (matchesRule(style.rule, changed.url)) {
            sections.push(i + 1);
            chrome.tabs.insertCSS(id, {"code": style.code, "allFrames": true, "runAt": "document_start"});
        }
    }
    if (sections.length > 0) {
        sendResultMessage("applied", changed.url, sections);
    } else {
        sendResultMessage("ignored", changed.url);
    }
}

// matchesRule returns true if any list of parts in rule matches url.
function matchesRule(rule, url) {
    var i, l;
    for (i = 0, l = rule.length; i < l; i += 1) {
        if (matches(rule[i], url)) {
            return true;
        }
    }
    return false;
}

// matches returns true if url contains every part in parts.
function matches(parts, url) {
    var i, l;
    for (i = 0, l = parts.length; i < l; i += 1) {
        if (url.indexOf(parts[i]) < 0) {
            return false;
        }
    }
    return true;
}

// send a message about the outcome of a restyle check.
function sendResultMessage(state, url, sections) {
    chrome.runtime.sendMessage({"type": "restyle", "state": state, "url": url, "sections": sections});
}
